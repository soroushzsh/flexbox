# Flexbox

simple codes to understand flexbox.


flex container become flexible :

display: flex | inline-flex

### Perfect centering:

`.flex-container {
	display: flex;
	justify-content: center;
	align-items: center;
}
`
## The flex properties for **container**:


#### flex-direction :
row | row-reverse | column | column-reverse

#### flex-wrap :
wrap | nowrap | wrap-reverse

#### flex-flow : 
shorthand for flex-direction and flex-flow.

flex-direction | flex-wrap

#### justify-content:

This defines the alignment along the main axis. 

center | space-between | space-around | flex-start | flex-end


#### align-items :	
items toward their parent.

flex-start | flex-end | center | stretch |
baseline

#### align-content :
content toward each other in parent.

flex-start | flex-end | center | stretch |
space-between | space-around

## The flex properties for **children**:

#### order: 
integer

#### flex-basis:
is like width in flexy way.

length | auto

#### flex-shrink: 
how much a flex item will shrink

The value must be a number, default value is 1

#### flex-grow:
how much a flex item will grow relative to the rest of the flex items.

The value must be a number, default value is 0.

#### flex: 
shorthand for flex-grow | flex-shrink | flex-basis

align-self: auto | flex-start | flex-end | center | baseline | stretch


